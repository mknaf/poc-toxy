const http = require("http");
const settings = require('./settings');

http.createServer(function(req, res) {
    setTimeout(function() {
        res.write(`The server delayed this response ${settings.server_delay}ms.`);
        res.end();
    }, settings.server_delay);
}).listen(settings.server_port);

# About

A PoC for [Toxy](https://github.com/h2non/toxy).

# Setup

Run `npm install`.

# Usage

1. Start the server instance via `node server.js`.
1. Start the proxy instance via `node proxy.js`.
1. Run a request against the server across the proxy via `node client.js`.

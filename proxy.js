var toxy = require("toxy");
var settings = require("./settings");

var poisons = toxy.poisons;
var rules = toxy.rules;

// Create a new toxy proxy
var proxy = toxy();

proxy
    // add a latency of 1000mx
    .poison(poisons.latency({ jitter: 1000 }))
    // with a chance of 1/2
    .rule(rules.probability(50));

// abort everything after 500ms
//proxy.poison(toxy.poisons.abort(500))

// the actual forwards
proxy
  .get('/*')
  .forward(`http://localhost:${settings.server_port}`);

// Default server to forward incoming traffic
proxy.forward(`http://localhost:${settings.server_port}`);

proxy.listen(settings.proxy_port);
console.log("Proxy listening on port:", settings.proxy_port);

const axios = require("axios");
const settings = require('./settings');

var starttime = new Date();
axios
    .get(`http://localhost:${settings.proxy_port}`)
    .then(response => {
        console.log(response.data);
        console.log(`Response actually took ${starttime - new Date()}ms.`)
    })
    .catch(error => {
        console.log(error);
    });
